::!/bin/bash

:: Copyright (C) 2019 ECORP SAS - Author: Romain Hunault
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: TODO: test if a device go from "sideload" mode to recovery

:: Parameter
:: $1: DEVICE_ID (optionnal)

:: Return
:: - displayed: DEVICE_ID device detected

:: Exit status
:: - 0 : New device detected
:: - 1 : Error
:: - 101 : DEVICES_LOCKED_PATH missing

set "DEVICE_ID=%1"

:: check device_ID est définit
if defined %DEVICE_ID (
adb -s %DEVICE_ID% wait-for-recovery
if not errorlevel 1 (
  adb -s %DEVICE_ID% shell twrp mount system
  exit /b 0
)
)


::While no devide_id defined: try to find one
:while-loop
for /F "tokens=*" %%a IN ('"adb get-serialno "') do (
SET "DEVICE_ID=%%a"
)
if not defined %DEVICE_ID (
  timeout 1 >nul
  goto :while-loop
) 


adb -s %DEVICE_ID% wait-for-recovery
if not errorLevel 1 ( adb -s %DEVICE_ID% shell twrp mount system )

echo %DEVICE_ID%
