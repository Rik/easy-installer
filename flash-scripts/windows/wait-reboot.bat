::!/bin/bash

:: Copyright (C) 2019 ECORP SAS - Author: Romain Hunault
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: $1: DEVICE_ID Device we are waiting for reboot

:: Exit status
:: - 0 : New device detected
:: - 1 : Error
:: - 10 : DEVICE_ID is missing
:: - 101 : DEVICE_ID is not detected

set "DEVICE_ID=%1"

if not defined %DEVICE_ID ( exit /b 10)

adb -s %DEVICE_ID% get-state 2>&1 | grep "recovery"
if errorLevel 1 (exit /b 101)

:wait-leave-recovery
adb -s ${DEVICE_ID} get-state
if %errorLevel% == 0
goto :wait-leave-recovery



call wait-leave-recovery


